import socket


if __name__ == '__main__':
    ip = '127.0.0.1'
    port = 9090

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.connect((ip, port))

    # отправка сообщения со стороны клиента на сервер
    message = input("Введите сообщение: ")
    server.send(bytes(message, "utf-8"))