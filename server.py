import socket

if __name__ == '__main__':
    ip = '127.0.0.1'
    port = 9090

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((ip, port))
    server.listen(5)

    while True:
        client, address = server.accept()
        print(f"Подключение установлено - {address[0]}:{address[1]}")

        #для получения сообщения на стороне сервера
        message = client.recv(1024)
        message = message.decode("utf-8")
        print(message)

        client.close()
